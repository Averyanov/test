function ready ( e )
{
	let items = document.getElementsByClassName('item');

	Array.prototype.forEach.call( items, eacher );
}

function eacher( v, i, a )
{
	v.addEventListener('click', clickOnItem.bind(a) );
}

function clickOnItem( e )
{
	let el = e.target;

	let bindClass = (el.classList.contains( 'col-md-2' ) || el.classList.contains( 'col-md-3' ))
		? nO 
		: nA

	Array.prototype.forEach.call( this, bindClass.bind(el) );	
}

function nO ( v,i,a ) // narrow others
{
	v.classList.remove( 'col-md-3' );
	v.classList.remove( 'col-md-4' );
	v.classList.remove( 'col-md-2' );

	if ( v != this )
	{
		v.classList.add( 'col-md-2' );
	}
	else
	{
		v.classList.toggle( 'col-md-4' );
	}
}

function nA ( v,i,a ) // narrow all
{
	v.classList.remove( 'col-md-2' );
	v.classList.remove( 'col-md-4' );
	v.classList.add( 'col-md-3' );
}

document.addEventListener("DOMContentLoaded", ready);